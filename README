Summary:
========
Extension to export objects to separate files for animation

Description:
============
Based on a request posted earlier this year on the mailing list [1] to
export paths created with the 'Interpolate' extension to individual SVG
files, I started to work on such an extension - out of curiosity,
mostly, to better understand limitations of the current extension
system.

Main features:
==============
The extension allows to export
- a selection of multiple objects, or
- the members of a single selected group, or
- all layers, or
- all sublayers of the current layer (or of all layers)
to separate files into a user-defined export directory. The exported
files can be saved as individual files, as TAR or as ZIP archive.

Supported file formats for export are:
- Inkscape SVG (the default)
- Plain SVG
- PNG
- GIF
- JPEG
- PDF

Additionally, if the chosen export format is a bitmap format, an
animated GIF can be created automatically (available options for the
animation are delay, loop and dispose).

Basic usage:
============
Select a group of paths (e.g. created with 'Extensions > Generate from
Path > Interpolate'), open 'Extensions > Export > Objects to Files',
adjust the export directory as needed and click on 'Apply'.

Known issues:
=============
1) Defs: The extension does not automatically copy just the required
shared resources into the exported SVG file. The user can decide -
depending on the content of the current document - whether or not to
include a full copy of the original <defs> section in each of the
exported SVG files, e.g. for path effects or gradient definitions (the
defs can be cleaned up later on if needed).

2) Clones: Object references across different layers, or among different
selected objects, are not resolved. This will cause orphaned clones (not
rendered visibly) in exported files (any format), if the original of a
clone is on a different layer, or not among the "other" content
(unselected, or 'Common layers') which can optionally be included in
each exported file.

3) Dependencies: Export to GIF/JPEG and the creation of the animated GIF
depend on external tools ('convert' from ImageMagick). Inkscape's binary
packages for Windows and OS X do not include a full installation of
ImageMagick, which will make these specific features unavailable in
out-of-the-box installations.

4) Popen() on Windows:
Using Popen() with 'shell=False' (the arguments are passed as list,
not as string) needs testing on Windows (the extensions currently
shipping with Inkscape use the (unsafe) 'shell=True' when running
external commands). While 'shell=False' works ok with commands which
spawn e.g. 'inkscape', it fails with commands which use ImageMagick's
'convert' [2] (the external command fails because Microsoft's system
tool 'convert.exe' is always first in the default search path). Not sure
yet how to solve this (I'd rather avoid falling back to 'shell=True';
specifying the full path to a default ImageMagick installation is not a
viable solution either (the default installation includes the version
number in the top-level directory)

TODO:
=====
- Win32: fix support for non-ASCII filenames in inkscape shell mode
  (affects all export formats except SVG if basename uses non-ASCII)
- Optionally use layer names for export filename (useful?)
- i18n (dialog as well as error messages)
- Documentation and basic tutorial
- ...

Ideas:
======
- Per-frame (layer) options for animation (similar to GIMP)
- Requested but not implemented: Integration with existing 'Interpolate'
  extension (this part of the original spec was dropped to be able to
  support more use cases than just the output of 'Interpolate')
- ...

Files:
======
Attached to this report [3] is a recent snapshot of the extension, the
changelog can be followed in this repository:
https://gitlab.com/su-v/inx-exportobjects
ZIP archives of releases are available here:
https://gitlab.com/su-v/inx-exportobjects/tags

References:
===========
[1] Specs as posted on the inkscape-devel mailing list:
http://sourceforge.net/p/inkscape/mailman/message/34592259/

[2]
http://stackoverflow.com/questions/24789749/popen-with-conflicting-executable-path

[3] Bug report (closed):
https://bugs.launchpad.net/bugs/1530380
